package ru.test.broadcastexamples

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class SMSReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        abortBroadcast()
        Log.d("pidor", "receive sms, abort broadcast is $abortBroadcast")
    }
}